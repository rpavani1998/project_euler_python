from math import sqrt

def isPrime(num):
	if(num == 2):
		return True
	elif(num % 2 == 0 or num == 1):
		return False
	else:
		for n in range(3,(int(sqrt(num))) + 1,2):
			if(num % n == 0):
				return False
	return True

def greatestPrimeFactor(num):
	n = num//2 + 1
	while(n > 1):
		if(num % n == 0  and isPrime(n)):
			return n
		n -= 1
	return -1

print(isPrime(25))
print(greatestPrimeFactor(600851475143))
