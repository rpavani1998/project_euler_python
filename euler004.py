def numReverse(num):
	reverse = 0
	while(num > 0):
		reminder = num %10
		reverse = (reverse *10) + reminder
		num = num //10
	return reverse

def isPalindrome(num):
	return ( num == numReverse(num))

def largestThreeDigitProductPalindrome():
	i = 999 
	j = 999
	palindromes = []
	while(i >= 100):
		j = 999
		while(j >= 100):
			if(isPalindrome(i * j)):
				palindromes.append(i*j)
			j -= 1
		i -= 1
	print(max(palindromes))

largestThreeDigitProductPalindrome()



